import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {
  WorkerData,
  MessageFromWorker,
  WorkerResult,
  OnFileLoadedData
} from './app.worker.model';
import * as GEO from './utils/distance.util';
import { delay } from './utils/util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  readonly MIN_LONGITUDE = GEO.MIN_LONGITUDE;
  readonly MAX_LONGITUDE = GEO.MAX_LONGITUDE;

  readonly MIN_LATITUDE = GEO.MIN_LATITUDE;
  readonly MAX_LATITUDE = GEO.MAX_LATITUDE;

  form: FormGroup;
  LATITUDE_VALIDATORS: ValidatorFn = Validators.compose([
    Validators.required,
    Validators.min(this.MIN_LATITUDE),
    Validators.max(this.MAX_LATITUDE)
  ]) as ValidatorFn;
  LONGITUDE_VALIDATORS: ValidatorFn = Validators.compose([
    Validators.required,
    Validators.min(this.MIN_LONGITUDE),
    Validators.max(this.MAX_LONGITUDE)
  ]) as ValidatorFn;

  // worker related vars
  activeWorker: Worker;
  selectedFile: File;
  calculatedRadius: number;
  // progress of calculation [0-100]
  progress: number = 0;
  results: WorkerResult[] = [];

  constructor(public formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      centerOfRadius: this.formBuilder.group({
        lon: [null, this.LONGITUDE_VALIDATORS],
        lat: [null, this.LATITUDE_VALIDATORS]
      }),
      borderOfRadius: this.formBuilder.group({
        lon: [null, this.LONGITUDE_VALIDATORS],
        lat: [null, this.LATITUDE_VALIDATORS]
      }),
      file: [null, Validators.required]
    });
  }

  ngOnInit() {}

  get matchedCount() {
    return this.results.filter(i => i.matched).length;
  }

  onFileSelect(target: EventTarget | null) {
    const files = (target as HTMLInputElement).files;
    if (files && files.length) {
      const fileToRead = files[0];
      this.selectedFile = fileToRead;
    }
  }

  submit() {
    if (this.activeWorker) {
      this.activeWorker.terminate();
    }

    this.progress = 0;
    this.results = [];

    const circleData = {
      centerOfRadius: this.form.value?.centerOfRadius,
      borderOfRadius: this.form.value?.borderOfRadius
    };

    // https://caniuse.com/webworkers -> web workers support is 98.42%
    if (typeof Worker !== 'undefined') {
      // Create a new worker
      const worker = new Worker(new URL('./app.worker', import.meta.url));
      this.activeWorker = worker;
      worker.onmessage = ({ data }: { data: MessageFromWorker }) => {
        console.log(`page got message: ${data}`);
        if (data.type === 'calculated_radius') {
          this.calculatedRadius = data.value;
        }
        if (data.type === 'progress') {
          this.progress = data.value;
        }
        if (data.type === 'results') {
          this.results.push(...data.value);
          console.log('results = ', this.results);
        }
      };

      let workerData: WorkerData = {
        circleData: circleData,
        file: this.selectedFile
      };
      worker.postMessage(workerData);
    } else {
      // Web workers are not supported in this environment.
      // Here i use the fallback so the program still executes correctly.
      const fileReader = new FileReader();
      fileReader.onload = event =>
        this.onFileLoaded(event, {
          circleData: circleData,
          file: this.selectedFile
        });
      fileReader.readAsText(this.selectedFile, 'UTF-8');
    }
  }

  stopWorker() {
    if (this.activeWorker) {
      this.activeWorker.terminate();
    }
  }

  isFinite(value) {
    return Number.isFinite(value);
  }

  // fallback logic (similar to the worker logic)
  async onFileLoaded(fileLoadedEvent: any, data: OnFileLoadedData) {
    const textFromFileLoaded = fileLoadedEvent.target.result;
    let circleData = data.circleData;

    let circleCentrLat = circleData.centerOfRadius.lat;
    let circleCentrLon = circleData.centerOfRadius.lon;

    let circleRadius = GEO.getDistanceFromLatLonInKm(
      circleCentrLat,
      circleCentrLon,
      circleData.borderOfRadius.lat,
      circleData.borderOfRadius.lon
    );

    this.calculatedRadius = circleRadius;

    let parsedCSV = GEO.parseCSV(textFromFileLoaded);

    let results: WorkerResult[] = [];
    for (let i = 0; i < parsedCSV.length; i++) {
      let latLon = parsedCSV[i];

      let latFromFile = Number(latLon[0].trim());
      let lonFromFile = Number(latLon[1].trim());

      let distanceBetweenPoints = GEO.getDistanceFromLatLonInKm(
        circleCentrLat,
        circleCentrLon,
        latFromFile,
        lonFromFile
      );

      results.push({
        lat: latFromFile,
        lon: lonFromFile,
        matched: distanceBetweenPoints < circleRadius,
        distanceInKm: distanceBetweenPoints
      });

      if ((i % 10 === 0 && i !== 0) || i === parsedCSV.length - 1) {
        this.progress = Math.floor((i / (parsedCSV.length - 1)) * 100);
        this.results.push(...results);
        results = [];
      }

      /**
       * need this because I don't want to freeze the interface
       * you can comment out the delay () code and add
       * for (let i = 0; i < 10000000; i++) {} above for the testing purpose
       * with delay () it works fine
       * without delay it will freeze the user interface
       */
      await delay(1);
    }
  }
}
