//worker input interface
export interface WorkerData {
  circleData: {
    centerOfRadius: {
      lon: number;
      lat: number;
    };
    borderOfRadius: {
      lon: number;
      lat: number;
    };
  };
  file: File;
}

// worker output types
interface WorkerCalculatedRadiusMessage {
  type: 'calculated_radius';
  value: number;
}

interface WorkerProgressMessage {
  type: 'progress';
  value: number;
}

export interface WorkerResult {
  lat: number;
  lon: number;
  matched: boolean;
  distanceInKm: number;
}

interface WorkerResultsMessage {
  type: 'results';
  value: WorkerResult[];
}

export type MessageFromWorker =
  | WorkerCalculatedRadiusMessage
  | WorkerProgressMessage
  | WorkerResultsMessage;

// since i need fallback in case the worker does not exists
// i use the similar to the worker logic inside the app.component
// but it's not the best idea to declare type as WorkerData in app.component,
// because it's not a worker.
// for this reason here i just created new type based on WorkerData
export type OnFileLoadedData = WorkerData;
