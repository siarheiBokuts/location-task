// Longitude : max/min 180.0000000 to -180.0000000
export const MAX_LONGITUDE = 180;
export const MIN_LONGITUDE = -180;

// Latitude : max/min 90.0000000 to -90.0000000
export const MAX_LATITUDE = 90;
export const MIN_LATITUDE = -90;

// formula https://www.movable-type.co.uk/scripts/latlong.html
export const getDistanceFromLatLonInKm = (
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number
) => {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
};

const deg2rad = (deg: number) => {
  return deg * (Math.PI / 180);
};

// code from https://stackoverflow.com/questions/1293147/example-javascript-code-to-parse-csv-data
export const parseCSV = (str: string): [[string, string]] => {
  var arr: any = [];
  var quote = false; // 'true' means we're inside a quoted field

  // Iterate over each character, keep track of current row and column (of the returned array)
  for (
    var rowIndex = 0, colIndex = 0, currentSymbolIndex = 0;
    currentSymbolIndex < str.length;
    currentSymbolIndex++
  ) {
    var cc = str[currentSymbolIndex],
      nc = str[currentSymbolIndex + 1]; // Current character, next character
    arr[rowIndex] = arr[rowIndex] || []; // Create a new row if necessary
    arr[rowIndex][colIndex] = arr[rowIndex][colIndex] || ''; // Create a new column (start with empty string) if necessary

    // If the current character is a quotation mark, and we're inside a
    // quoted field, and the next character is also a quotation mark,
    // add a quotation mark to the current column and skip the next character
    if (cc == '"' && quote && nc == '"') {
      arr[rowIndex][colIndex] += cc;
      ++currentSymbolIndex;
      continue;
    }

    // If it's just one quotation mark, begin/end quoted field
    if (cc == '"') {
      quote = !quote;
      continue;
    }

    // If it's a comma and we're not in a quoted field, move on to the next column
    if (cc == ',' && !quote) {
      ++colIndex;
      continue;
    }

    // If it's a newline (CRLF) and we're not in a quoted field, skip the next character
    // and move on to the next row and move to column 0 of that new row
    if (cc == '\r' && nc == '\n' && !quote) {
      ++rowIndex;
      colIndex = 0;
      ++currentSymbolIndex;
      continue;
    }

    // If it's a newline (LF or CR) and we're not in a quoted field,
    // move on to the next row and move to column 0 of that new row
    if (cc == '\n' && !quote) {
      ++rowIndex;
      colIndex = 0;
      continue;
    }
    if (cc == '\r' && !quote) {
      ++rowIndex;
      colIndex = 0;
      continue;
    }

    // Otherwise, append the current character to the current column
    arr[rowIndex][colIndex] += cc;
  }
  return arr;
};
