import { TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import * as GEO from './utils/distance.util';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule
      ],
      declarations: [AppComponent]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('GEO.getDistanceFromLatLonInKm should works correctly with given args (1)', () => {
    const result = GEO.getDistanceFromLatLonInKm(
      43.15172986920992,
      -160.7870501992893,
      79.15962265609252,
      -159.55658149591542
    );

    console.log(
      'GEO.getDistanceFromLatLonInKm should works correctly with given args (1) result = ',
      result,
      'km'
    );
    expect(result).toBeGreaterThan(4003);
    expect(result).toBeLessThan(4005);
  });

  it('GEO.getDistanceFromLatLonInKm should works correctly with given args (2)', () => {
    const result = GEO.getDistanceFromLatLonInKm(89, 179, -89, -179);

    console.log(
      'GEO.getDistanceFromLatLonInKm should works correctly with given args (2) result = ',
      result,
      'km'
    );
    expect(result).toBeGreaterThan(19789);
    expect(result).toBeLessThan(19793);
  });
});
